/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrec02;
import javax.swing.JOptionPane;
import java.awt.Dimension;
/**
 *
 * @author safoe
 */
public class jintNomina extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintNomina
     */
    public jintNomina() {
        initComponents();
        this.deshabilitar();
    }
    public void deshabilitar(){
        this.txtNombre.setEnabled(false);
        this.txtDias.setEnabled(false);
        this.txtNumRecibo.setEnabled(false);
        
        this.btmMostrar.setEnabled(false);
        this.btmGuardar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtNombre.setEnabled(true);
        this.txtDias.setEnabled(true);
        this.txtNumRecibo.setEnabled(true);
        
        this.btmGuardar.setEnabled(true);
    }
    
    public void limpiar(){
        this.txtDias.setText("");
        this.txtNombre.setText("");
        this.txtNumRecibo.setText("");
        
        this.txtCalcularImpuesto.setText("");
        this.txtCalcularPago.setText("");
        this.txtCalcularTotal.setText("");
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtCalcularTotal = new javax.swing.JTextField();
        txtCalcularPago = new javax.swing.JTextField();
        txtCalcularImpuesto = new javax.swing.JTextField();
        txtNumRecibo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDias = new javax.swing.JTextField();
        cmbNivel = new javax.swing.JComboBox<>();
        cmbPuesto = new javax.swing.JComboBox<>();
        btmCancelar = new javax.swing.JButton();
        btmNuevo = new javax.swing.JButton();
        btmGuardar = new javax.swing.JButton();
        btmMostrar = new javax.swing.JButton();
        btmLimpiar = new javax.swing.JButton();
        btmCerrar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Dias trabajados:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 270, 170, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Num. Recibo:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 30, 130, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Nombre:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 80, 130, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Puesto:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 130, 130, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("Nivel:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 190, 130, 30);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pagos", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Tahoma", 1, 48))); // NOI18N
        jPanel1.setLayout(null);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("Calcular total:");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(30, 190, 150, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("Calcular pago:");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(40, 80, 150, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("Calcular impuesto:");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(30, 140, 190, 30);

        txtCalcularTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCalcularTotalActionPerformed(evt);
            }
        });
        jPanel1.add(txtCalcularTotal);
        txtCalcularTotal.setBounds(160, 190, 210, 40);

        txtCalcularPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCalcularPagoActionPerformed(evt);
            }
        });
        jPanel1.add(txtCalcularPago);
        txtCalcularPago.setBounds(170, 80, 210, 40);

        txtCalcularImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCalcularImpuestoActionPerformed(evt);
            }
        });
        jPanel1.add(txtCalcularImpuesto);
        txtCalcularImpuesto.setBounds(210, 140, 210, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(20, 360, 540, 250);

        txtNumRecibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumReciboActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumRecibo);
        txtNumRecibo.setBounds(150, 20, 210, 40);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(110, 80, 210, 40);

        txtDias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiasActionPerformed(evt);
            }
        });
        getContentPane().add(txtDias);
        txtDias.setBounds(190, 270, 90, 40);

        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nivel 1", "Nivel 2" }));
        cmbNivel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbNivelActionPerformed(evt);
            }
        });
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(90, 190, 120, 30);

        cmbPuesto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Auxiliar", "Albañil", "Ing. Obra" }));
        getContentPane().add(cmbPuesto);
        cmbPuesto.setBounds(110, 130, 120, 30);

        btmCancelar.setText("Cancelar");
        btmCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCancelar);
        btmCancelar.setBounds(410, 620, 140, 30);

        btmNuevo.setText("Nuevo");
        btmNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btmNuevo);
        btmNuevo.setBounds(590, 40, 140, 40);

        btmGuardar.setText("Guardar");
        btmGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btmGuardar);
        btmGuardar.setBounds(590, 130, 140, 40);

        btmMostrar.setText("Mostrar");
        btmMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmMostrar);
        btmMostrar.setBounds(590, 180, 140, 40);

        btmLimpiar.setText("Limpiar");
        btmLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btmLimpiar);
        btmLimpiar.setBounds(40, 620, 140, 30);

        btmCerrar.setText("Cerrar ");
        btmCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btmCerrar);
        btmCerrar.setBounds(230, 620, 140, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCalcularTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCalcularTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCalcularTotalActionPerformed

    private void txtNumReciboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumReciboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumReciboActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtDiasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiasActionPerformed

    private void txtCalcularPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCalcularPagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCalcularPagoActionPerformed

    private void txtCalcularImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCalcularImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCalcularImpuestoActionPerformed

    private void cmbNivelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbNivelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbNivelActionPerformed

    private void btmCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btmCancelarActionPerformed

    private void btmLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btmLimpiarActionPerformed

    private void btmCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmCerrarActionPerformed
        // TODO add your handling code here:
        int opcion=0;
        opcion=JOptionPane.showConfirmDialog(this,"Deseas salir ","registro de venta",JOptionPane.YES_NO_OPTION);
        if (opcion==JOptionPane.YES_OPTION){
        this.dispose();
    }
    }//GEN-LAST:event_btmCerrarActionPerformed

    private void btmNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmNuevoActionPerformed
        // TODO add your handling code here:
        nom=new Nomina();
        this.habilitar();
        
    }//GEN-LAST:event_btmNuevoActionPerformed

    private void btmGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito = false;
        if (this.txtNumRecibo.getText().equals(""))exito=true;
        if (this.txtNombre.getText().equals(""))exito=true;
        if (this.txtDias.getText().equals(""))exito=true;
        if(this.cmbPuesto.getSelectedIndex()==3)exito=true;
        if(this.cmbNivel.getSelectedIndex()==2)exito=true;
        
        if (exito==true){
            JOptionPane.showMessageDialog(this, "Falta capturar informacion");
           }
        else{
            nom.setNombre(this.txtNombre.getText());
            nom.setNumRecibo(Integer.parseInt(this.txtNumRecibo.getText()));
            nom.setDias(Integer.parseInt(this.txtDias.getText()));
            nom.setNivel(this.cmbNivel.getSelectedIndex());
            nom.setPuesto(this.cmbPuesto.getSelectedIndex());
            JOptionPane.showMessageDialog(this,"Se Guardo Correctamente la Información");
            this.btmMostrar.setEnabled(true);
        }
        
        
        
    }//GEN-LAST:event_btmGuardarActionPerformed

    private void btmMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmMostrarActionPerformed
        // TODO add your handling code here:
        this.txtNumRecibo.setText(String.valueOf(nom.getNumRecibo()));
        this.txtDias.setText(String.valueOf(nom.getDias()));
        this.txtNombre.setText(nom.getNombre());
        
        this.cmbNivel.setSelectedIndex(nom.getNivel());
        this.cmbPuesto.setSelectedIndex(nom.getPuesto());
        
        
         this.txtCalcularPago.setText(String.valueOf(nom.CalcularPago()));
         this.txtCalcularImpuesto.setText(String.valueOf(nom.CalcularImpuesto()));
         this.txtCalcularTotal.setText(String.valueOf(nom.CalcularTotal()));
        
        
        
    }//GEN-LAST:event_btmMostrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btmCancelar;
    private javax.swing.JButton btmCerrar;
    private javax.swing.JButton btmGuardar;
    private javax.swing.JButton btmLimpiar;
    private javax.swing.JButton btmMostrar;
    private javax.swing.JButton btmNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JComboBox<String> cmbPuesto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtCalcularImpuesto;
    private javax.swing.JTextField txtCalcularPago;
    private javax.swing.JTextField txtCalcularTotal;
    private javax.swing.JTextField txtDias;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumRecibo;
    // End of variables declaration//GEN-END:variables
private Nomina nom;
}
